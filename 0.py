import os
os.chdir('/home')
#creating the directory in a cheaty way :):)
os.system('mkdir -p $HOME/os_lab_0')
#this part is to take the user's login to create folder under '/home/$USER'
username=os.getlogin()
#assigning the absolute path of the os_lab_0 directory into variable
oslabpath='/home/'+username+'/os_lab_0/'
os.chdir(oslabpath)
#creating the files under the current directory if they do not exist
open(oslabpath+'selamlar.txt', 'a').close()
open(oslabpath+'onurhocam.txt', 'a').close()
open(oslabpath+'umutben.py', 'a').close()
#getting the status of the files
lastselam= os.stat('selamlar.txt')
lasthocam= os.stat('onurhocam.txt')
lastumut= os.stat('umutben.py')
#assigning the modification time of the file into the file & printing them
lastselam=format(lastselam.st_mtime)
lasthocam=format(lasthocam.st_mtime)
lastumut=format(lastumut.st_mtime)

print("Last modification time for selamlar.txt: " + lastselam)
print("Last modification time for onurhocam.txt: " + lasthocam)
print("Last modification time for umutben.py: " + lastumut)
#getting the contents of the dir as a list
filesunderoslab=os.listdir(oslabpath)
#printing every file that ends with '.txt'
for i in filesunderoslab:
	if i[-4:]=='.txt':
		print(i)
